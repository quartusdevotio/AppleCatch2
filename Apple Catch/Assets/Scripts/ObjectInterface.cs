using System;

namespace AppleCatch.Interface
{
    public interface INormalObject
    {
        void AddScore();
        public event Action<int> OnAddScore;
        public event Action<int> OnMissApple;

        void SubtractLife();
    }
}