using UnityEngine;

namespace AppleCatch.Player
{
    //Referensi masukin ke GameManager
    public class PlayerController : MonoBehaviour
    {
        public float movementSpeed = 5f;
        private Rigidbody2D rigidBody2D;
        private Vector2 movementDirection;

        public float jumpForce = 2f;
        private bool onGround;
        [SerializeField] private float fallMultiplier = 0.2f;

        public delegate void PlayerDeadEventHandler();
        public event PlayerDeadEventHandler PlayerDead;
        public bool isDead { get; private set; }

        // Start is called before the first frame update
        private void Start()
        {
            rigidBody2D = GetComponent<Rigidbody2D>();
            PlayerDead += OnPlayerDead;
        }

        // Update is called once per frame
        private void Update()
        {
            if(!isDead)
            {
                TakeInput();
            }
        }

        private void FixedUpdate()
        {
            Move();

            if(isDead)
            {
                rigidBody2D.velocity = Vector2.up * Physics2D.gravity.y * fallMultiplier;
            }
        }

        private void TakeInput()
        {
            var horizontalMovement = Input.GetAxisRaw("Horizontal");
            var verticalMovement = Input.GetAxisRaw("Vertical");
            movementDirection = new Vector2(horizontalMovement, verticalMovement);

            if(onGround && movementDirection.y >= 0.01f)
            {
                Jump();
            }
        }

        private void Move()
        {
            if(onGround && movementDirection.y == 0f)
            {
                rigidBody2D.velocity = new Vector2(movementDirection.x * movementSpeed, 0f);
            }
            else
            {
                rigidBody2D.velocity = new Vector2(movementDirection.x * movementSpeed, rigidBody2D.velocity.y);
            }
        }

        private void Jump()
        {
            rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, 0f);
            rigidBody2D.velocity += new Vector2(0f, 1f * jumpForce);
        }

        public void OnPlayerDead()
        {
            rigidBody2D.velocity = new Vector2(0f, 0f);
            movementDirection = new Vector2(0f, 0f);
            rigidBody2D.bodyType = RigidbodyType2D.Kinematic;
            isDead = true;
        }

        public void SetOnGround(bool onGround)
        {
            this.onGround = onGround;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("ItemDestroyer"))
            {
                PlayerDead();
            }
        }
    }
}
