using UnityEngine;

namespace AppleCatch.Manager
{
    public class LifeManager : MonoBehaviour
    {
        private int _life = 3;
        public int Life 
        {
            get { return _life; }
        }

        public delegate void PlayerTakeDamageEventHandler();
        public event PlayerTakeDamageEventHandler PlayerTakeDamage;

        private EdgeCollider2D collider2D;

        private void Awake()
        {
            collider2D = GetComponent<EdgeCollider2D>();
        }

        public void SubtractLife(int damage)
        {
            if (_life == 0)
            {
                return;
            }

            _life -= damage;
            PlayerTakeDamage();
        }

        public void DisableCollider()
        {
            collider2D.enabled = false;
        }
    }
}