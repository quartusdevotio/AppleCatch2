using System;
using UnityEngine;

namespace AppleCatch.Objects
{
    public abstract class Item : MonoBehaviour
    {
        
        protected Rigidbody2D rigidBody2D;
        [SerializeField] protected float fallMultiplier = 0.2f;

        private void FixedUpdate()
        {
            rigidBody2D.velocity += Vector2.up * Physics2D.gravity.y * fallMultiplier * Time.deltaTime;
        }
    }
}