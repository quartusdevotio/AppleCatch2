using System;
using UnityEngine;

namespace AppleCatch.Manager
{
    public class ScoreManager : MonoBehaviour
    {
        private int score = 0;

        //public delegate void ScoreChangedEventHandler();
        //public event ScoreChangedEventHandler ScoreChanged;
        public event Action<int> ScoreChanged;

        public void AddScore(int value)
        {
            score += value;
            ScoreChanged?.Invoke(score);
        }

        public int GetScore()
        {
            int output = score;
            return output;
        }
    }
}
