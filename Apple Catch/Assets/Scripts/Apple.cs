using AppleCatch.Manager;
using AppleCatch.Interface;
using UnityEngine;
using System;

namespace AppleCatch.Objects
{
    public class Apple : Item, INormalObject
    {
        public event Action<int> OnAddScore;
        public event Action<int> OnMissApple;

        private void Start()
        {
            rigidBody2D = GetComponent<Rigidbody2D>();
        }

        private void OnDestroy()
        {
            OnAddScore = null;
        }

        public void AddScore()
        {
            OnAddScore?.Invoke(1);
            AudioManager.instance.PlaySfx("PickUp", 1f);
            Destroy(gameObject);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                AddScore();
            }
            else if (collision.CompareTag("ItemDestroyer"))
            {
                Destroy(gameObject);
            }
        }

        public void SubtractLife()
        {
            
        }
    }
}
